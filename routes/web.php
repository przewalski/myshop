<?php

Auth::routes();

Route::get('','HomepageController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/redirect/{provider}', 'SocialController@redirect');

Route::get('/callback/{provider}', 'SocialController@callback');

Route::get('/product/{id}','ProductController@index');

Route::get('/shop','ShopController@index');

Route::get('/shop/{name}','ShopController@catalogItem');

Route::get('/allProducts', 'ShopController@allProducts');

Route::get('/allCatalog', 'ShopController@allCatalog');

Route::get('/allBrand', 'ShopController@allBrand');

Route::middleware(['auth'])->group(function () {

    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('/profile', 'ProfileController@profile');

    Route::post('/profileUpdate', 'ProfileController@profileUpdate');

    Route::get('/allUserDelivered', 'ProfileController@allUserDelivered');

    Route::get('/allUserOrdered', 'ProfileController@allUserOrdered');

    Route::get('/cart','CartController@cart');

    Route::post('/add-to-cart/{id}','CartController@addOneItem');

    Route::post('/changeAmount/{id}','CartController@changeAmount');

    Route::post('/favorite/{id}', 'CartController@favorite');

    Route::get('/checkOrItemInFavorite/{id}','WishListController@checkOrItemInFavorite');

    Route::post('/cart/deleteOneItem/{id}','CartController@deleteOneItem');

    Route::get('/cart/delete','CartController@deleteAll');

    Route::get('/cart/pay','CartController@getPay');

    Route::post('/cart/checkout', 'CartController@postPay');

    Route::get('/wishlist','WishListController@index');

    Route::post('/wishlist/delete/{id}','WishListController@deleteOneItem');

    Route::get('/wishlist/delete','WishListController@delete');

    Route::get('/wishlist/getAddToCart','WishListController@getAddToCartAll');

    Route::post('/addToCartProduct/{id}','ProductController@addToCartProduct');

    Route::get('/allProductsEvenDeleted', 'AdminController@allProductsEvenDeleted');

    Route::group(['middleware' => ['admin']], function () {

        Route::get('/admin', 'AdminController@admin');

        Route::get('/allUsers', 'AdminController@allUsers');

        Route::post('/userUpdate/{id}', 'AdminController@userUpdate');

        Route::get('/userBlock/{id}', 'AdminController@userBlock');

        Route::get('/userUnBlock/{id}', 'AdminController@userUnBlock');

        Route::get('/userDelete/{id}', 'AdminController@userDelete');

        Route::post('/addBrand', 'AdminController@addBrand');

        Route::post('/addCatalogItem', 'AdminController@addCatalogItem');

        Route::post('/addProduct', 'AdminController@addProduct');

        Route::post('/changeBrandName/{id}', 'AdminController@changeBrandName');

        Route::post('/changeCatalogName/{id}', 'AdminController@changeCatalogName');

        Route::post('/changeProduct/{id}', 'AdminController@changeProduct');

        Route::get('/deleteBrand/{id}', 'AdminController@deleteBrand');

        Route::get('/deleteCatalog/{id}', 'AdminController@deleteCatalog');

        Route::get('/deleteProduct/{id}', 'AdminController@deleteProduct');

        Route::get('/allOrdersInCart', 'AdminController@allOrdersInCart');

        Route::get('/allOrdersOrdered', 'AdminController@allOrdersOrdered');

        Route::get('/orderVerified/{id}', 'AdminController@orderVerified');

        Route::get('/allOrdersVerified', 'AdminController@allOrdersVerified');

        Route::get('/orderDelivered/{id}', 'AdminController@orderDelivered');

        Route::get('/allOrdersDelivered', 'AdminController@allOrdersDelivered');
    });
});