/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'

Vue.use(VueRouter);

let routes = [
    { path: '/users', component: require('./components/Control/Users.vue').default},
    { path: '/addToProducts', component: require('./components/Add to/Products.vue').default},
    { path: '/addToCatalog', component: require('./components/Add to/Catalog.vue').default},
    { path: '/addToBrands', component: require('./components/Add to/Brands.vue').default},
    { path: '/brands', component: require('./components/Control/Brands.vue').default},
    { path: '/catalog', component: require('./components/Control/Catalog.vue').default},
    { path: '/products', component: require('./components/Control/Products.vue').default},
    { path: '/ordersInCart', component: require('./components/Orders/InCart.vue').default},
    { path: '/ordersOrdered', component: require('./components/Orders/Ordered.vue').default},
    { path: '/ordersVerified', component: require('./components/Orders/Verified.vue').default},
    { path: '/ordersDelivered', component: require('./components/Orders/Delivered.vue').default},
    { path: '/information', component: require('./components/User/Information.vue').default},
    { path: '/userOrdered', component: require('./components/User/Ordered.vue').default},
    { path: '/userDelivered', component: require('./components/User/Delivered.vue').default},
];

const router = new VueRouter({
    routes // short for `routes: routes`
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('product_items', require('./components/ProductItems.vue').default);
Vue.component('product', require('./components/Product.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
