@extends('layouts')

@section('head')
    <link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
@stop


@section('content')
    <!-- WishList -->

    <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">
                        <div class="cart_title">Wishlist</div>
                        <div class="cart_items">
                            <ul class="cart_list">
                                @foreach($productfavorites as $productfavorite)
                                    <li class="cart_item clearfix" id="{{$productfavorite->Product->id}}">
                                        <div class="cart_item_image"><img src="/uploads/{{$productfavorite->Product->file_name}}" alt=""></div>
                                        <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                            <div class="cart_item_name cart_info_col">
                                                <div class="cart_item_title">Name</div>
                                                <div class="cart_item_text">{{$productfavorite->Product->name}}</div>
                                            </div>
                                            <div class="cart_item_price cart_info_col">
                                                <div class="cart_item_title">Price</div>
                                                <div class="cart_item_text">{{$productfavorite->Product->price}} ‎€</div>
                                            </div>
                                            <div class="cart_item_add cart_info_col">
                                                <div class="cart_item_text"><button class="btn btn-success" type="button">Add to cart</button></div>
                                            </div>
                                            <div class="cart_item_remove cart_info_col">
                                                <div class="cart_item_text"><button class="btn btn-danger" type="button">Remove</button></div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- Wishlist Total -->

                    <div class="cart_buttons">
                        <a href="/wishlist/getAddToCart"><button type="button" class="button cart_button_checkout wishlist_button" style="background-color: #38c172">Add all to cart</button></a>
                        <a href="/wishlist/delete"><button type="button" class="button cart_button_checkout" style="background-color: red">Clear wishlist</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/wishlist.js"  async></script>
@stop

