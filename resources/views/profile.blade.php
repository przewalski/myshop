@extends('layouts')

@section('head')
    <link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
@stop

@section('content')
    <div class="container" id="app">
        <div class="row">
            <div class="col-3">
                <div class="nav flex-column nav-pills" aria-orientation="vertical">
                    <router-link class="nav-link active" data-toggle="pill" to="/information">My profile</router-link>
                    <router-link class="nav-link" data-toggle="pill" to="/userOrdered">Ordered</router-link>
                    <router-link class="nav-link" data-toggle="pill" to="/userDelivered">Delivered</router-link>
                </div>
            </div>
            <div class="col">
                <router-view :users="{{$user}}"></router-view>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/app.js"></script>
@stop