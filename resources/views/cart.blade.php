@extends('layouts')

@section('head')
    <link rel="stylesheet" type="text/css" href="/styles/cart_styles.css">
    <link rel="stylesheet" type="text/css" href="/styles/cart_responsive.css">
    <link rel="stylesheet" type="text/css" href="/styles/main_styles.css">
@stop

@section('content')
    @if(Session::has('success'))
    <div class="row" style="padding-left: 40%">
        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
            <div class="alert alert-dismissible fade show" role="alert">
                <div id="charge-message" class="alert alert-success">
                    <strong>{{ Session::get('success') }}</strong>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
        @endif
    <!-- Cart -->

    <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">
                        <div class="cart_title">Shopping Cart</div>
                        <div class="cart_items">
                            <ul class="cart_list">
                                @foreach($productorders as $productorder)
                                <li class="cart_item clearfix" id="{{$productorder->id}}">
                                    <div class="cart_item_image"><img src="/uploads/{{$productorder->product->file_name}}" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text">{{$productorder->product->name}}</div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                             <!-- <div class="cart_item_text"></div> -->
                                            <div class="cart_item_text"><input class="cart-quantity-input" type="number" value="{{$productorder->amount}}"></div>
                                        </div>
                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text">{{$productorder->product->price}} ‎€</div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text">{{$productorder->product->price * $productorder->amount}} ‎€</div>
                                        </div>
                                        <div class="cart_item_remove cart_info_col">
                                            <div class="cart_item_text"><button class="btn btn-danger" type="button">REMOVE</button></div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                        <!-- Order Total -->
                        <div class="order_total">
                            <div class="order_total_content text-md-right">
                                <div class="order_total_title">Order Total:</div>
                                <div class="order_total_amount">{{App\Helpers\Cart::sum()}} ‎€</div>
                            </div>
                        </div>

                        <div class="cart_buttons">
                            <a href="/cart/delete"><button type="button" class="button cart_button_clear">Clear cart</button></a>
                            <a href="/cart/pay"><button type="button" class="button cart_button_checkout">Pay</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="/js/cart_custom.js"></script>
    <script src="/js/store.js"  async></script>
@stop