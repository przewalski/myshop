@extends('layouts')

@section('head')


    <link rel="stylesheet" type="text/css" href="/styles/product_styles.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@stop

@section('content')
    <!-- Single Product -->

    <div class="single_product">
        <div class="container">
            <div class="row">

                <!-- Selected Image -->
                <div class="col-lg-5 order-lg-2 order-1">
                    <div class="image_selected"><img src="/uploads/{{$product->file_name}}" alt=""></div>
                </div>

                <!-- Description -->
                <div class="col-lg-5 order-3">
                    <div class="product_description">
                        <div class="product_category">{{$product->ProductCatalog->name}}</div>
                        <div class="product_name"  id="{{$product->id}}">{{$product->name}}</div>
                        <div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div>
                        <div class="product_text"><p>{{$product->about}}</p></div>
                        <div class="order_info d-flex flex-row">
                            <form action="#">
                                <div class="clearfix" style="z-index: 1000;">

                                @if(!Auth::guest())
                                    <!-- Product Quantity -->
                                    <div class="product_quantity clearfix">
                                        <span>Quantity: </span>
                                        <input id="quantity_input" type="text" pattern="[0-9]*" value="1">
                                    </div>
                                </div>
                                @endif

                                <div class="product_price">{{$product->price}} ‎€</div>
                                @if(!Auth::guest())
                                <div class="button_container">
                                    <button type="button" class="button cart_button">Add to Cart</button>
                                    <div  id="favorite" class="product_fav @if(!Auth::guest()) @if(count($product->ProductFavorite->where('user_id', $user->id)->where('product_id', $product->id))) active @endif @endif"><i class="fas fa-heart"></i></div>
                                </div>
                                    @else
                                    <div class="button_container">
                                        <a href="/login"><button type="button" class="button cart_button">Add to Cart</button></a>
                                        <a href="/login"><div class="product_fav"><i class="fas fa-heart"></i></div></a>
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/product_custom.js"></script>
    <script src="/js/product.js" async></script>
@stop