<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="OneTech shop project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="/styles/bootstrap4/bootstrap.min.css">
    <link href="/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/styles/cart_styles.css">
    <link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="/plugins/OwlCarousel2-2.2.1/animate.css">
    <link rel="stylesheet" type="text/css" href="/styles/cart_responsive.css">
</head>

<body>

<div class="super_container" id="app">

    <!-- Header -->

    <header class="header">

        <!-- Top Bar -->

        <div class="top_bar">
            <div class="container">
                <div class="row">
                    <div class="col d-flex flex-row">
                        <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="/images/phone.png" alt=""></div>+370 (699) 80 241</div>
                        <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="/images/mail.png" alt=""></div><a href="mailto:fastsales@gmail.com">przewalskip@gmail.com</a></div>
                        <div class="top_bar_content ml-auto">
                            <div class="top_bar_menu">
                                <ul class="standard_dropdown top_bar_dropdown">
                                    <li>
                                        <a href="#">English<i class="fas fa-chevron-down"></i></a>
                                    </li>
                                    <li>
                                        <a href="#">€ Euro<i class="fas fa-chevron-down"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="top_bar_user">
                                <div class="user_icon"><img src="/images/user.svg" alt=""></div>
                                @if(Auth::guest())
                                    <div><a href="/register">Register</a></div>
                                    <div><a href="/login">Sign in</a></div>
                                @else
                                    <div class="auth_id" id="{{$user->id}}"><a href="">{{$user->name}}</a></div>
                                    <div><a href="/logout">Logout</a></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <!-- Main Navigation -->

        <nav class="main_nav" style="background: black; margin-top: 30px">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"><div class="logo"><a href="https://myshop.com/">MyShop</a></div></div>
                    <div class="col-8">
                        <div class="main_nav_content d-flex flex-row">

                            <div class="cat_menu_container">
                                <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                                    <div class="cat_menu_text">admin panel</div>
                                </div>
                            </div>

                            <!-- Main Nav Menu -->

                            <div class="main_nav_menu ml-auto">
                                <ul class="standard_dropdown main_nav_dropdown">
                                    <li><router-link to="/users">Users<i class="fas fa-chevron-down"></i></router-link></li>
                                    <li class="hassubs">
                                        <router-link to="/products">Products<i class="fas fa-chevron-down"></i></router-link>
                                        <ul>
                                            <li><router-link to="/addToProducts">Add product<i class="fas fa-chevron-down"></i></router-link></li>
                                        </ul>
                                    </li>
                                    <li class="hassubs">
                                        <router-link to="/catalog">Catalog<i class="fas fa-chevron-down"></i></router-link>
                                        <ul>
                                            <li><router-link to="/addToCatalog">Add catalog item<i class="fas fa-chevron-down"></i></router-link></li>
                                        </ul>
                                    </li>
                                    <li class="hassubs">
                                        <router-link to="/brands">Brands<i class="fas fa-chevron-down"></i></router-link>
                                        <ul>
                                            <li><router-link to="/addToBrands">Add brand<i class="fas fa-chevron-down"></i></router-link></li>
                                        </ul>
                                    </li>
                                    <li><router-link to="/ordersInCart">Orders<i class="fas fa-chevron-down"></i></router-link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <br>
    <br>
    <br>
    <br>

    <router-view></router-view>

</div>
<script src="/js/app.js"></script>
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/styles/bootstrap4/popper.js"></script>
<script src="/styles/bootstrap4/bootstrap.min.js"></script>
<script src="/plugins/greensock/TweenMax.min.js"></script>
<script src="/plugins/greensock/TimelineMax.min.js"></script>
<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="/plugins/greensock/animation.gsap.min.js"></script>
<script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="/plugins/easing/easing.js"></script>
<script src="/js/notify.js"></script>
<script src="/plugins/slick-1.8.0/slick.js"></script>



<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>

</html>