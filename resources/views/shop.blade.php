@extends('layouts')

@section('head')
    <link rel="stylesheet" type="text/css" href="/plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/styles/shop_styles.css">
    <link rel="stylesheet" type="text/css" href="/styles/shop_responsive.css">
@stop

@section('content')
    <!-- Home -->

    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/shop_background.jpg"></div>
        <div class="home_overlay"></div>
        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Shop</h2>
        </div>
    </div>

    <!-- Shop -->

    <div class="shop" id="app">
        <product_items :points="{{$products}}" @if(!Auth::guest()) :user="{{$user}}" @endif></product_items>
    </div>

@stop

@section('script')
    <script src="/js/app.js"></script>
    <script src="/plugins/Isotope/isotope.pkgd.min.js"></script>
    <script src="/plugins/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="/js/shop_custom.js"></script>
@stop
