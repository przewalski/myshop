<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([ProductCatalogsTableSeeder::class, BrandsTableSeeder::class, ProductsTableSeeder::class, ProductOrderStatusesSeeder::class, ProductOrdersSeeder::class]);
    }
}
