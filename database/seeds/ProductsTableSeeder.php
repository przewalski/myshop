<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'product_catalog_id' => 2,
                'name' => 'Macbook',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '1500',
                'views' => '0',
                'file_name' => 'single_2.jpg'
            ],[
                'product_catalog_id' => 3,
                'name' => 'Macbook Pro',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 4,
                'price' => '1000',
                'views' => '0',
                'file_name' => 'single_1.jpg'
            ],[
                'product_catalog_id' => 4,
                'name' => 'Mouse',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 5,
                'price' => '15',
                'views' => '0',
                'file_name' => 'new_7.jpg'
            ],[
                'product_catalog_id' => 5,
                'name' => 'Xiomi',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 2,
                'price' => '500',
                'views' => '0',
                'file_name' => 'best_1.png'
            ],[
                'product_catalog_id' => 6,
                'name' => 'Planszet',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '200',
                'views' => '0',
                'file_name' => 'best_6.png'
            ],[
                'product_catalog_id' => 7,
                'name' => 'Pult',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 5,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_6.jpg'
            ],[
                'product_catalog_id' => 8,
                'name' => 'Ps4',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 4,
                'price' => '400',
                'views' => '0',
                'file_name' => 'view_5.jpg'
            ],[
                'product_catalog_id' => 1,
                'name' => 'Nauszniki',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 9,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_1.jpg'
            ],[
                'product_catalog_id' => 2,
                'name' => 'Macbook',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '1500',
                'views' => '0',
                'file_name' => 'single_2.jpg'
            ],[
                'product_catalog_id' => 3,
                'name' => 'Macbook Pro',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 4,
                'price' => '1000',
                'views' => '0',
                'file_name' => 'single_1.jpg'
            ],[
                'product_catalog_id' => 4,
                'name' => 'Mouse',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 5,
                'price' => '15',
                'views' => '0',
                'file_name' => 'new_7.jpg'
            ],[
                'product_catalog_id' => 5,
                'name' => 'Xiomi',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 2,
                'price' => '500',
                'views' => '0',
                'file_name' => 'best_1.png'
            ],[
                'product_catalog_id' => 6,
                'name' => 'Planszet',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '200',
                'views' => '0',
                'file_name' => 'best_6.png'
            ],[
                'product_catalog_id' => 7,
                'name' => 'Pult',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 5,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_6.jpg'
            ],[
                'product_catalog_id' => 8,
                'name' => 'Ps4',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 4,
                'price' => '400',
                'views' => '0',
                'file_name' => 'view_5.jpg'
            ],[
                'product_catalog_id' => 1,
                'name' => 'Nauszniki',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 9,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_1.jpg'
            ],[
                'product_catalog_id' => 2,
                'name' => 'Macbook',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '1500',
                'views' => '0',
                'file_name' => 'single_2.jpg'
            ],[
                'product_catalog_id' => 3,
                'name' => 'Macbook Pro',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 4,
                'price' => '1000',
                'views' => '0',
                'file_name' => 'single_1.jpg'
            ],[
                'product_catalog_id' => 4,
                'name' => 'Mouse',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 5,
                'price' => '15',
                'views' => '0',
                'file_name' => 'new_7.jpg'
            ],[
                'product_catalog_id' => 5,
                'name' => 'Xiomi',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 2,
                'price' => '500',
                'views' => '0',
                'file_name' => 'best_1.png'
            ],[
                'product_catalog_id' => 6,
                'name' => 'Planszet',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 2,
                'amount' => 2,
                'price' => '200',
                'views' => '0',
                'file_name' => 'best_6.png'
            ],[
                'product_catalog_id' => 7,
                'name' => 'Pult',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 3,
                'amount' => 5,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_6.jpg'
            ],[
                'product_catalog_id' => 8,
                'name' => 'Ps4',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 4,
                'amount' => 4,
                'price' => '400',
                'views' => '0',
                'file_name' => 'view_5.jpg'
            ],[
                'product_catalog_id' => 1,
                'name' => 'Nauszniki',
                'about' => 'ofodsfosdfosdfosdofsdofsofsodfdsofosdfosd',
                'brand_id' => 5,
                'amount' => 9,
                'price' => '100',
                'views' => '0',
                'file_name' => 'view_1.jpg'
            ]
        ]);
    }
}
