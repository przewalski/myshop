<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductOrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_order_statuses')->insert([
            [
                'name' => 'InCart'
            ],[
                'name' => 'Ordered'
            ],[
                'name' => 'Verified'
            ],[
                'name' => 'Delivered'
            ]
        ]);
    }
}
