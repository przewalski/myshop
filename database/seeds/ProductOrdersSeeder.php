<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_orders')->insert([
            [
                'user_id' => 1,
                'product_id' => 1,
                'product_order_status_id' => 1,
                'amount' => 1
            ],[
                'user_id' => 1,
                'product_id' => 2,
                'product_order_status_id' => 2,
                'amount' => 1
            ],[
                'user_id' => 1,
                'product_id' => 3,
                'product_order_status_id' => 3,
                'amount' => 1
            ],[
                'user_id' => 1,
                'product_id' => 5,
                'product_order_status_id' => 4,
                'amount' => 1
            ]
        ]);
    }
}
