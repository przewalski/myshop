<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            [
                'name' => 'All brands'
            ],
            [
                'name' => 'Apple'
            ],[
                'name' => 'Samsung'
            ],[
                'name' => 'Intel'
            ],[
                'name' => 'Philips'
            ]
        ]);
    }
}
