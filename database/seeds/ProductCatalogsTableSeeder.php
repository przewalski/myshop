<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_catalogs')->insert([
            [
                'name' => 'All products',
                'amount' => NULL
            ],
            [
                'name' => 'Computers',
                'amount' => 0
            ],[
                'name' => 'Laptops',
                'amount' => 0
            ],[
                'name' => 'Hardware',
                'amount' => 0
            ],[
                'name' => 'Smartphones',
                'amount' => 0
            ],[
                'name' => 'Tablets',
                'amount' => 0
            ],[
                'name' => 'TV',
                'amount' => 0
            ],[
                'name' => 'Video Games',
                'amount' => 0
            ]
        ]);
    }
}
