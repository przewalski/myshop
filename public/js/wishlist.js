if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready();
}

function ready() {
    let removeWishListItemButtons = document.getElementsByClassName('btn-danger');
    for (let i = 0; i < removeWishListItemButtons.length; i++) {
        let button = removeWishListItemButtons[i];
        button.addEventListener('click', removeWishListItem)
    }

    let addToCartButtons = document.getElementsByClassName('btn-success');
    for (let i = 0; i < addToCartButtons.length; i++) {
        let button = addToCartButtons[i];
        button.addEventListener('click', addToCartClicked)
    }
}

function removeWishListItem(event) {
    let buttonClicked = event.target;
    let id = buttonClicked.parentElement.parentElement.parentElement.parentElement.id;
    buttonClicked.parentElement.parentElement.parentElement.parentElement.remove();
    $.ajax({
        url     : '/wishlist/delete/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateTotal(null,response);
            flashMessage(response);
        }
    })
}

function addToCartClicked(event) {
    let button = event.target;
    let id = button.parentElement.parentElement.parentElement.parentElement.id;
    let price = parseFloat(button.parentElement.parentElement.parentElement.getElementsByClassName('cart_item_price')[0].getElementsByClassName('cart_item_text')[0].innerText.replace('‎€',''));
    $.ajax({
        url     : '/add-to-cart/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateTotal(price,response);
            flashMessage(response);
        }
    })
}

function updateTotal(price,which) {
     if(which === "deleteOneItemSuccessfully") {
            document.getElementsByClassName('wishlist_count')[0].innerText--;
     } else if(which === "changeAmountSuccessfully" || which === "addToCartOneItemSuccessfully") {
        document.getElementsByClassName('cart_count_now')[0].innerText++;
        let priceNew = document.getElementsByClassName('cart_price')[0].innerText.replace('‎€','');
        priceNew = parseInt(priceNew) + price;
        document.getElementsByClassName('cart_price')[0].innerText = priceNew + ' ‎€';
    }
}

function flashMessage(which) {
    if(which === "deleteOneItemSuccessfully") {
        $.notify("Successfully! Deleted from wishlist!", "error");
    } else if(which === "changeAmountSuccessfully" || which === "addToCartOneItemSuccessfully") {
        $.notify("Successfully! Added to cart!", "success");
    }
}