if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready();
}

function ready() {
    let addToCartButtons = document.getElementsByClassName('cart_button')[0];
    addToCartButtons.addEventListener('click', addToCartClicked);

    let addToFavorite = document.getElementsByClassName('product_fav')[0];
    addToFavorite.addEventListener('click', addToFavoriteClicked);

}

function addToCartClicked() {
    const id = document.getElementsByClassName('product_name')[0].id;
    const price = parseFloat(document.getElementsByClassName('product_price')[0].innerText.replace('‎€',''));
    let amount = parseInt(document.getElementById('quantity_input').value);
    if (isNaN(amount) || amount <= 0 ) {
        amount = 1
    }
    $.ajax({
        url     : '/addToCartProduct/'+id,
        method  : 'post',
        data    : {amount : amount},
        success : function (response) {
            updateTotal(price,amount,response);
            flashMessage(response,amount);
        }
    })
}

function addToFavoriteClicked() {
    const id = document.getElementsByClassName('product_name')[0].id;
    $.ajax({
        url     : '/favorite/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateTotal(null,null,response);
            flashMessage(response);
        }
    })
}

function updateTotal(price,amount,which) {
    if(which === "changeAmountSuccessfully" || which === "addToCartOneItemSuccessfully") {
        document.getElementsByClassName('cart_count_now')[0].innerText = parseInt(document.getElementsByClassName('cart_count_now')[0].innerText) + amount;
        let priceNew = document.getElementsByClassName('cart_price')[0].innerText.replace('‎€','');
        priceNew = parseInt(priceNew) + price * amount;
        document.getElementsByClassName('cart_price')[0].innerText = priceNew + ' ‎€';
        document.getElementById('quantity_input').value = 1;

    } else if(which === "addToFavoriteSuccessfully") {
        document.getElementsByClassName('wishlist_count')[0].innerText++;
        document.getElementById("favorite").classList.add('active');

    } else if(which === "deletedFromFavoriteSuccessfully") {
        document.getElementsByClassName('wishlist_count')[0].innerText--;
        document.getElementById("favorite").classList.remove('active');
    }
}

function flashMessage(which, amount) {
    if(which === "addToCartOneItemSuccessfully") {
        $.notify("Successfully! Added to cart!", "success");

    } else if(which === "changeAmountSuccessfully") {
        $.notify("Successfully! Added +" + amount + " to cart", "info");

    } else if(which === "addToFavoriteSuccessfully"){
        $.notify("Successfully! Added to favorite cart!", "info");

    } else if(which === "deletedFromFavoriteSuccessfully"){
        $.notify("Successfully! Deleted from favorite cart!", "error");
    }
}