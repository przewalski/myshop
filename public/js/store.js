if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready();
}

function ready() {
    let removeCartItemButtons = document.getElementsByClassName('btn-danger');
    for (let i = 0; i < removeCartItemButtons.length; i++) {
        let button = removeCartItemButtons[i];
        button.addEventListener('click', removeCartItem)
    }

    let quantityInputs = document.getElementsByClassName('cart_item_quantity');
    for (let i = 0; i < quantityInputs.length; i++) {
        let input = quantityInputs[i];
        input.addEventListener('change', quantityChanged)
    }

    let addToCartButtons = document.getElementsByClassName('product_cart_button');
    for (let i = 0; i < addToCartButtons.length; i++) {
        let button = addToCartButtons[i];
        button.addEventListener('click', addToCartClicked)
    }

    let addToFavorite = document.getElementsByClassName('product_fav');
    for (let i = 0; i < addToFavorite.length; i++) {
        let favorite = addToFavorite[i];
        favorite.addEventListener('click', addToFavoriteClicked)
    }
}

function removeCartItem(event) {
    let buttonClicked = event.target;
    let id = buttonClicked.parentElement.parentElement.parentElement.parentElement.id;
    buttonClicked.parentElement.parentElement.parentElement.parentElement.remove();
    $.ajax({
        url     : '/cart/deleteOneItem/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateCartTotal();
            flashMessage(response);
        }
    });
}

function quantityChanged(event) {
    let input = event.target;
    if (isNaN(input.value) || input.value <= 0 ) {
        input.value = 1
    }
    let id = input.parentElement.parentElement.parentElement.parentElement.id;
    let amount = input.parentElement.getElementsByClassName('cart-quantity-input')[0].value;
    $.ajax({
        url     : '/changeAmount/'+id,
        method  : 'post',
        data    : {amount : amount},
        success : function (response) {
            updateCartTotal();
            flashMessage(response);
        }
    });
}

function addToCartClicked(event) {
    let button = event.target;
    let shopItem = button.parentElement.parentElement;
    let id = shopItem.id;
    let price = parseFloat(shopItem.getElementsByClassName('product_price')[0].innerText.replace('‎€',''));
    $.ajax({
        url     : '/add-to-cart/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateTotal(price,response);
            flashMessage(response);
        }
    });
}

function addToFavoriteClicked(event) {
    let favorite = event.target;
    let id = favorite.id;
    $.ajax({
        url     : '/favorite/'+id,
        method  : 'post',
        data    : {},
        success : function (response) {
            updateTotal(null,response);
            flashMessage(response);
        }
    })
}

function updateCartTotal() {
    let cartItemContainer = document.getElementsByClassName('cart_items')[0];
    let cartRows = cartItemContainer.getElementsByClassName('cart_item');
    let total = 0;
    let totalAmount = 0;
    for (let i = 0; i < cartRows.length; i++) {
        let cartRow = cartRows[i];
        let priceElement = cartRow.getElementsByClassName('cart_item_price')[0];
        priceElement = priceElement.getElementsByClassName('cart_item_text')[0];
        let quantityElement = cartRow.getElementsByClassName('cart_item_quantity')[0];
        quantityElement = quantityElement.getElementsByClassName('cart-quantity-input')[0];
        let price = parseFloat(priceElement.innerText.replace('‎€',''));
        let quantity = quantityElement.value;
        let totalItem = cartRow.getElementsByClassName('cart_item_total')[0];
        totalItem = totalItem.getElementsByClassName('cart_item_text')[0].innerText = price*quantity + ' ‎€';;
        total = total + (price * quantity);
        totalAmount = totalAmount + quantity*1;
    }
    total = Math.round(total * 100) / 100;
    document.getElementsByClassName('order_total_amount')[0].innerText = total + ' ‎€';
    document.getElementsByClassName('cart_count_now')[0].innerText = totalAmount;
    document.getElementsByClassName('cart_price')[0].innerText = total + ' ‎€';
}

function updateTotal(price,which) {
    if(which === "changeAmountSuccessfully" || which === "addToCartOneItemSuccessfully") {
        document.getElementsByClassName('cart_count_now')[0].innerText++;
        let priceNew = document.getElementsByClassName('cart_price')[0].innerText.replace('‎€','');
        priceNew = parseInt(priceNew) + price;
        document.getElementsByClassName('cart_price')[0].innerText = priceNew + ' ‎€';

    } else if(which === "addToFavoriteSuccessfully") {
        document.getElementsByClassName('wishlist_count')[0].innerText++;

    } else if(which === "deletedFromFavoriteSuccessfully") {
        document.getElementsByClassName('wishlist_count')[0].innerText--;
    }
}

function flashMessage(which) {
    if(which === "deleteOneItemSuccessfully") {
        $.notify("Successfully! Deleted from cart!", "error");

    } else if(which === "changeAmountSuccessfully" || which === "addToCartOneItemSuccessfully") {
        $.notify("Successfully! Added to cart!", "success");

    } else if(which === "changedAmountSuccessfully"){
        $.notify("Successfully! Changed quantity!", "info");

    } else if(which === "addToFavoriteSuccessfully"){
        $.notify("Successfully! Added to favorite cart!", "info");

    } else if(which === "deletedFromFavoriteSuccessfully"){
        $.notify("Successfully! Deleted from favorite cart!", "error");
    }
}

