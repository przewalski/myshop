<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, SparkPost and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1226106231110642',
        'client_secret' => '1ba58cf70b2a42fe96a882d0a37f33bf',
        'redirect' => 'https://myshop.com/callback/facebook',
    ],

    'google' => [
        'client_id' => '1095718858653-1rdji3l8l5kflpdeshb84k18pf8053nn.apps.googleusercontent.com',
        'client_secret' => '3Gqy6JF3KjavuHgrEkQM6CSN',
        'redirect' => 'https://myshop.com/callback/google',
    ],

    'stripe' => [
        'model'  => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
];
