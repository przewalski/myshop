<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'product_catalog_id', 'name', 'about', 'brand_id', 'amount', 'price', 'views', 'file_name', 'delete'
    ];

    public function productCatalog()
    {
        return $this->belongsTo('App\Models\Product\Catalog');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Product\Brand');
    }

    public function productFavorite()
    {
        return $this->hasMany('App\Models\Product\Favorite');
    }

    public function productFile()
    {
        return $this->hasMany('App\Models\Product\File');
    }
}
