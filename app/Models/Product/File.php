<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'product_files';

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
