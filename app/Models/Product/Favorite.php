<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'product_favorites';

    protected $fillable = [
        'user_id', 'product_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}