<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'product_order_statuses';
}
