<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'product_orders';

    protected $fillable = [
        'user_id', 'product_id', 'product_order_status_id', 'amount'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function productStatus()
    {
        return $this->belongsTo('App\Models\Product\Status');
    }

}
