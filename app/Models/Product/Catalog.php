<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $table = 'product_catalogs';

    protected $fillable = [
        'name'
    ];

    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }
}
