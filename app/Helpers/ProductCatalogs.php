<?php

namespace App\Helpers;

use App\Models\Product\Catalog;

/**
 * Class Roles
 *
 * @package App\Helpers
 */
class ProductCatalogs
{
    /**
     * @return mixed
     */
    static function all()
    {
        return Catalog::all();
    }
}