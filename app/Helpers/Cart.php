<?php

namespace App\Helpers;


use App\Models\Product\Order;
use Illuminate\Support\Facades\Auth;

/**
 * Class Roles
 *
 * @package App\Helpers
 */
class Cart
{

    static function amount()
    {
        $user = Auth::user();
        $productorders = Order::where('user_id', $user->id)->where( 'product_order_status_id', 1)->get();
        $amount = 0;
        foreach ($productorders as $productorder)
        {
            $amount = $amount + $productorder->amount;
        }
        return $amount;
    }

    static function sum()
    {
        $user = Auth::user();
        $productorders = Order::where('user_id', $user->id)->where( 'product_order_status_id', 1)->get();
        $totalprice = 0;
        foreach ($productorders as $productorder)
        {
            $totalprice = $totalprice + $productorder->amount * $productorder->product->price;
        }
        return $totalprice;
    }
}
