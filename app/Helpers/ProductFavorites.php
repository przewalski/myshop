<?php

namespace App\Helpers;

use App\Models\Product\Favorite;
use Illuminate\Support\Facades\Auth;

/**
 * Class Roles
 *
 * @package App\Helpers
 */
class ProductFavorites
{
    /**
     * @return mixed
     */
    static function amount()
    {
        $user = Auth::user();
        $favoritesAmount = Favorite::where('user_id', $user->id)->get();
        $amount = count($favoritesAmount);
        return $amount;
    }
}