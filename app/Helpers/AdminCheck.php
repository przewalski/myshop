<?php

namespace App\Helpers;


use App\Models\Admin;
use Illuminate\Support\Facades\Auth;

/**
 * Class Roles
 *
 * @package App\Helpers
 */
class AdminCheck
{

    static function admin()
    {
        $orAdmin = false;
        $admins = Admin::all();
        foreach ($admins as $admin)
        {
            if($admin->user_id === Auth::user()->id){
                $orAdmin = true;
                return $orAdmin;
            }
        }
        return $orAdmin;
    }

}
