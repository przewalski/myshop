<?php

namespace App\Helpers;

use App\Models\Product;

/**
 * Class Roles
 *
 * @package App\Helpers
 */
class Products
{
    /**
     * @return mixed
     */
    static function all()
    {
        return Product::where('delete', false)->get();
    }

    static function amount()
    {
        $products = Product::where('delete', false)->get();
        $amount = 0;
        foreach ($products as $product)
        {
            $amount = $amount + $product->amount;
        }
        return $amount;
    }
}