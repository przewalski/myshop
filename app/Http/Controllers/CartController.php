<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Product\Order;
use App\Models\User;
use App\Models\Product\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Stripe\Stripe;
use Stripe\Charge;
use App\Helpers\Cart;
class CartController extends Controller
{

    public function cart()
    {
        $user = Auth::user();
        $productorders = Order::where('user_id', $user->id)->where( 'product_order_status_id', 1)->get();
        return view('cart',compact('user', 'productorders'));
    }

    public function deleteOneItem($id) {
        Order::where('id', $id)->where('user_id', Auth::user()->id)->delete();
        return 'deleteOneItemSuccessfully';
    }

    public function deleteAll()
    {
        Order::where('user_id', Auth::user()->id)->where( 'product_order_status_id', 1)->delete();
        return redirect('https://myshop.com/');
    }

    public function addOneItem($id) {
        $productOrder = Order::where('user_id', Auth::user()->id)->where('product_id', $id)->where('product_order_status_id',1)->first();
        if(!empty($productOrder)) {
            $productOrder->update(['amount' => ++$productOrder->amount]);
            return 'changeAmountSuccessfully';
        }
        else {
            $product = Product::where('delete', false)->where('id',$id)->first();
            Order::create([
                'user_id' => Auth::user()->id,
                'product_id' => $product->id,
                'product_order_status_id' => 1,
                'amount' => 1
            ]);
            return 'addToCartOneItemSuccessfully';
        }
    }

    public function changeAmount($id) {
        if(isset($_POST['amount']) && $_POST['amount'] > 0) {
            $amount = $_POST['amount'];
            $productOrder = Order::where('id', $id)->where('product_order_status_id',1)->first();
            if(Auth::user()->id === $productOrder->user_id) {
                $productOrder->update(['amount' => $amount]);
                return 'changedAmountSuccessfully';
            }
            else {
                return 'YouCantChangeOther';
            }
        }
    }

    public function favorite($id) {
        $favorite = Favorite::where('user_id', Auth::user()->id)->where('product_id', $id)->first();
        if(!empty($favorite)) {
            Favorite::where('id', $favorite->id)->delete();
            return 'deletedFromFavoriteSuccessfully';
        }
        else {
            Favorite::create([
                'user_id' => Auth::user()->id,
                'product_id' => $id
            ]);
            return 'addToFavoriteSuccessfully';
        }
    }

    public function getPay()
    {
        $user = Auth::user();
        return view('pay',compact('user'));
    }

    public function postPay(Request $request)
    {
        $productOrders = Order::where('user_id', Auth::user()->id)->where( 'product_order_status_id', 1)->get();

        Stripe::setApiKey('sk_test_7JwKynscQZFk4DFXqLNaayE9007SpvEaJP');

        try {
            Charge::create(array(
                "amount" => Cart::sum()*100,
                "currency" => "eur",
                "source" => 'tok_visa',
                "description" => "Text Charge"
            ));
            foreach ($productOrders as $productOrder) {
                $productOrder->update(['product_order_status_id' => 2]);
            }
        } catch (\Exception $e) {
            return redirect('cart/pay')->with('error', $e->getMessage());
        }

        Session::forget('cart');
        return redirect('cart')->with('success', 'Successfully purchased products!');
    }

}
