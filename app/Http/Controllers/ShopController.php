<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Product\Catalog;
use App\Models\Product\Brand;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class ShopController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $products = Product::where('delete', false)->get();
        return view('shop',compact('user','products'));
    }

    public function catalogItem($name)
    {
        $user = Auth::user();
        if($name === 'All products') {
            return redirect('/shop');
        } else {
            $catalog = Catalog::where('name', $name)->first();
            $products = Product::where('product_catalog_id', $catalog->id)->where('delete', false)->get();
            return view('shopCatalog',compact('user','products', 'catalog'));
        }
    }

    public function allProducts() {
        return Product::where('delete', false)->get();
    }

    public function allCatalog() {
        return Catalog::all();
    }

    public function allBrand() {
        return Brand::all();
    }
}
