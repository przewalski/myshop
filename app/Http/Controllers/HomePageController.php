<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product\Catalog;

class HomePageController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $productCatalogs = Catalog::all();
        return view('welcome',compact('user', 'productCatalogs'));
    }
}
