<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Product\Catalog;
use App\Models\Product\Favorite;
use App\Models\Product\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishListController extends Controller
{
    public function index() {
        $user = Auth::user();
        $productfavorites = Favorite::where('user_id', Auth::user()->id)->get();
        return view('wishlist',compact('user', 'productfavorites'));
    }

    public function deleteOneItem($id) {
        Favorite::where('product_id', $id)->where('user_id', Auth::user()->id)->delete();
        return 'deleteOneItemSuccessfully';
    }

    public function delete() {
        Favorite::where('user_id', Auth::user()->id)->delete();
        return redirect('https://myshop.com/');
    }

    public function getAddToCartAll() {
        $productfavorites = Favorite::where('user_id', Auth::user()->id)->get();
        foreach ($productfavorites as $productfavorite){
            $productOrder = Order::where('user_id', Auth::user()->id)->where('product_id', $productfavorite->product_id)->where('product_order_status_id', 1)->first();
            if(!empty($productOrder)) {
                $productOrder->update(['amount' => ++$productOrder->amount]);
            }
            else {
                 Order::create([
                    'user_id' => Auth::user()->id,
                    'product_id' => $productfavorite->product_id,
                    'product_order_status_id' => 1,
                    'amount' => 1
                ]);
            }
        }
        return redirect('cart')->with('success', 'Successfully added all to cart!');
    }

    public function checkOrItemInFavorite($id) {
        $product = Product::findOrFail($id);
        if(count($product->ProductFavorite->where('user_id', Auth::user()->id)->where('product_id', $product->id))){
            return 'true';
        }
        else {
            return 'false';
        }
    }
}
