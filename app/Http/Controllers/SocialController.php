<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\Models\User;
class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->user();
        $user = $this->createUser($getInfo,$provider);
        auth()->login($user);
        return redirect()->to('https://myshop.com/');
    }
    function createUser($getInfo,$provider){
        $user = User::where('facebook_id', $getInfo->id)->orWhere( 'google_id', $getInfo->id)->first();
        if(!$user) {
            $user = User::where('email', $getInfo->email)->first();
            if($user) {
                if($provider == 'facebook') {
                    $user->update([
                        'facebook_id' => $getInfo->id
                    ]);
                }
                else {
                    $user->update([
                        'google_id' => $getInfo->id
                    ]);
                }
            }
            else {
                if ($provider == 'facebook') {
                    $user = User::create([
                        'name'     => $getInfo->name,
                        'email'    => $getInfo->email,
                        'facebook_id' => $getInfo->id
                    ]);
                }
                else {
                    $user = User::create([
                        'name' => $getInfo->name,
                        'email' => $getInfo->email,
                        'google_id' => $getInfo->id
                    ]);
                }
            }
        }
        return $user;
    }
}