<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileRequest;
use App\Models\Product\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function profile() {
        $user = Auth::user();
        return view('profile',compact('user'));
    }

    public function profileUpdate(CreateProfileRequest $request) {
        Auth::user()->update($request->all());
        return 'Successfully updated profile!';
    }

    public function allUserDelivered() {
        return Order::where('user_id', Auth::user()->id)->where('product_order_status_id', 4)->get();
    }

    public function allUserOrdered() {
        return Order::where('user_id', Auth::user()->id)->whereIn('product_order_status_id', [2,3])->get();
    }
}
