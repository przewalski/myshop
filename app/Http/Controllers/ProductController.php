<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Product\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index($id)
    {
        $user = Auth::user();
        $product = Product::where('delete', false)->where('id', $id)->first();
        return view('product',compact('user', 'product'));
    }

    public function addToCartProduct($id) {

        if(isset($_POST['amount']) && $_POST['amount'] > 0) {
            $amount = $_POST['amount'];
            $productOrder = Order::where('user_id', Auth::user()->id)->where('product_id', $id)->where('product_order_status_id',1)->first();

            if(!empty($productOrder)) {
                $amount = $amount + $productOrder->amount;
                $productOrder->update(['amount' => $amount]);
                return 'changeAmountSuccessfully';
            }
            else {
                $product = Product::findOrFail($id);
                Order::create([
                    'user_id' => Auth::user()->id,
                    'product_id' => $product->id,
                    'product_order_status_id' => 1,
                    'amount' => $amount
                ]);
                return 'addToCartOneItemSuccessfully';
            }
        }
    }
}
