<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBrandRequest;
use App\Http\Requests\CreateCatalogItemRequest;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\CreateProfileRequest;
use App\Models\Product;
use App\Models\Product\Brand;
use App\Models\Product\Catalog;
use App\Models\Product\Favorite;
use App\Models\Admin;
use App\Models\Product\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function admin() {
        $user = Auth::user();
        return view('admin',compact('user'));
    }

    //adding brands, catalog items, products

    public function addBrand(CreateBrandRequest $request) {
        Brand::create($request->all());
    }

    public function addCatalogItem(CreateCatalogItemRequest $request) {
        Catalog::create($request->all());
    }

    public function addProduct(CreateProductRequest $request) {

        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists. ";
            $uploadOk = 0;
        }
    // Check file size
        if ($_FILES["file"]["size"] > 500000) {
            echo "Sorry, your file is too large. ";
            $uploadOk = 0;
        }
    // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. ";
            $uploadOk = 0;
        }
    // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded. ";
    // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                Product::create([
                    'name' => $request->input('name'),
                    'product_catalog_id' => $request->input('product_catalog_id'),
                    'about' => $request->input('about'),
                    'brand_id' => $request->input('brand_id'),
                    'amount' => $request->input('amount'),
                    'price' => $request->input('price'),
                    'views' => 0,
                    'file_name' => $_FILES['file']['name']
                ]);
                echo 'successfully';
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

    //adding brands, catalog items, products

    public function changeBrandName($id, CreateBrandRequest $request) {
        if($id == 1) {
            echo "You cant't changed brand: All brands";
        } else {
            Brand::findOrFail($id)->update($request->all());
            echo "Successfully updated!";
        }
    }

    public function changeCatalogName($id, CreateCatalogItemRequest $request) {
        if($id == 1) {
            echo "You cant't changed catalog item: All brands";
        } else {
           Catalog::findOrFail($id)->update($request->all());
            echo "Successfully updated!";
        }
    }

    public function changeProduct($id, CreateProductRequest  $request) {
        if(isset($_FILES["file"])) {
            $target_dir = "uploads/";
            $target_file = $target_dir.basename($_FILES["file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists. ";
                $uploadOk = 0;
            }
    // Check file size
            if ($_FILES["file"]["size"] > 500000) {
                echo "Sorry, your file is too large. ";
                $uploadOk = 0;
            }
    // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. ";
                $uploadOk = 0;
            }
    // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded. ";
    // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    $product = Product::findOrFail($id);
                    $product->update([
                        'name' => $request->input('name'),
                        'product_catalog_id' => $request->input('product_catalog_id'),
                        'about' => $request->input('about'),
                        'brand_id' => $request->input('brand_id'),
                        'amount' => $request->input('amount'),
                        'price' => $request->input('price'),
                        'file_name' => $_FILES['file']['name']
                    ]);
                    unlink("uploads/".$_POST['file_name']);
                    echo 'Successfully updated!';
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        } else {
            $product = Product::where('delete', false)->where('id',$id)->first();
            $product->update($request->all());
            echo 'Successfully updated!';
        }
    }

    //delete brands, catalog items, products

    public function deleteBrand($id) {
        if($id == 1) {
            echo "You cant't delete brand: All brands";
        } else {
            $products = Product::where('brand_id', $id)->where('delete', false)->get();
            foreach ($products as $product) {
                $product->update(['brand_id' => 1]);
            }
            Brand::where('id', $id)->delete();
            return "Successfully deleted!";
        }
    }

    public function deleteCatalog($id) {
        if($id == 1) {
            echo "You cant't delete catalog item: All products";
        } else {
            $products = Product::where('product_catalog_id', $id)->where('delete', false)->get();
            foreach ($products as $product) {
                $product->update(['product_catalog_id' => 1]);
            }
            Catalog::where('id', $id)->delete();
            return "Successfully deleted!";
        }
    }

    public function deleteProduct($id) {
        Favorite::where('product_id', $id)->delete();
        Order::where('product_id', $id)->where('product_order_status_id', 1)->delete();
        $product = Product::where('delete', false)->where('id',$id)->first();
        $product->update([
            'delete' => 1
        ]);
        return "Successfully deleted!";
    }

    //get all product even deleted

    public function allProductsEvenDeleted()
    {
        return Product::all();
    }

    //orders control

    public function allOrdersInCart()
    {
        return Order::where('product_order_status_id', 1)->get();
    }

    public function allOrdersOrdered()
    {
        return Order::where('product_order_status_id', 2)->get();
    }

    public function allOrdersVerified() {
        return Order::where('product_order_status_id', 3)->get();
    }

    public function allOrdersDelivered()
    {
        return Order::where('product_order_status_id', 4)->get();
    }

    public function orderVerified($id) {
        Order::where('id',$id)->update(['product_order_status_id' => 3]);
        return 'Successfully!';
    }

    public function orderDelivered($id) {
        Order::where('id',$id)->update(['product_order_status_id' => 4]);
        return 'Successfully!';
    }

    //user control

    public function allUsers() {
        return User::all();
    }

    public function userUpdate($id, CreateProfileRequest $request) {
        User::where('id', $id)->update($request->all());
        return "Successfully updated!";
    }

    public function userBlock($id) {
        User::where('id', $id)->update([
            'block' => true
        ]);
        return "Successfully blocked!";
    }

    public function userUnBlock($id) {
        User::where('id', $id)->update([
            'block' => false
        ]);
        return "Successfully unblocked!";
    }

    public function userDelete($id) {
        User::where('id', $id)->delete();
        return "Successfully deleted!";
    }
}
