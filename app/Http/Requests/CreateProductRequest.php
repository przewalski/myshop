<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'product_catalog_id' => ['required','min:1'],
            'about' => 'required',
            'brand_id' => ['required', 'min:1'],
            'amount' => ['required', 'min:0','not_in:0'],
            'price' => ['required', 'min:0','not_in:0'],
        ];
    }
}
